```
     __      __   ___ _  __
 ___/ /___  / /_ / _/(_)/ /___  ___
/ _  // _ \/ __// _// // // -_)(_-<
\_,_/ \___/\__//_/ /_//_/ \__//___/

These are my configuration files for various common apps.

```

# How the "dotfiles" command works

When [dotfiles](bin/dotfiles) is run for the first time, it does a few things:

1. In Ubuntu, Git is installed if necessary via APT.
1. This repo is cloned into your user directory, under `~/.dotfiles`.
1. Files in `/copy` are copied into `~/`. ([read more](#the-copy-step))
1. Files in `/link` are symlinked into `~/`. ([read more](#the-link-step))
1. You are prompted to choose scripts in `/init` to be executed. The installer attempts to only select relevant scripts, based on the detected OS and the script filename.
1. Your chosen init scripts are executed (in alphanumeric order, hence the funky names). ([read more](#the-init-step))

On subsequent runs, step 1 is skipped, step 2 just updates the already-existing repo, and step 5 remembers what you selected the last time. The other steps are the same.

## Other subdirectories

* The `/backups` directory gets created when necessary. Any files in `~/` that would have been overwritten by files in `/copy` or `/link` get backed up there.
* The `/bin` directory contains executable shell scripts (including the [dotfiles][dotfiles] script) and symlinks to executable shell scripts. This directory is added to the path.
* The `/caches` directory contains cached files, used by some scripts or functions.
* The `/conf` directory just exists. If a config file doesn't **need** to go in `~/`, reference it from the `/conf` directory.
* The `/source` directory contains files that are sourced whenever a new shell is opened (in alphanumeric order, hence the funky names).
* The `/test` directory contains unit tests for especially complicated bash functions.
* The `/vendor` directory contains third-party libraries.

## The "copy" step
Any file in the `/copy` subdirectory will be copied into `~/`. Any file that _needs_ to be modified with personal information (like [copy/.gitconfig](copy/.gitconfig) which contains an email address and private key) should be _copied_ into `~/`. Because the file you'll be editing is no longer in `~/.dotfiles`, it's less likely to be accidentally committed into your public dotfiles repo.

## The "link" step
Any file in the `/link` subdirectory gets symlinked into `~/` with `ln -s`. Edit one or the other, and you change the file in both places. Don't link files containing sensitive data, or you might accidentally commit that data! If you're linking a directory that might contain sensitive data (like `~/.ssh`) add the sensitive files to your [.gitignore](.gitignore) file!

## The "init" step
Scripts in the `/init` subdirectory will be executed. A whole bunch of things will be installed, but _only_ if they aren't already.

* APT packages and git-extras via the [init/20_ubuntu_apt.sh](init/20_ubuntu_apt.sh) script
* Install zsh plugin via the [init/50_zsh_install.sh](init/50_zsh_install.sh) script

# Installation

**Warning you're not me, this script can be _very messy_ on your computer !**

1. Open a terminal and run :

```sh
bash -c "$(curl -fsSL https://gitlab.com/wboubi/dotfiles/raw/master/bin/dotfiles)" && zsh
```

There's a lot of stuff that requires admin access via `sudo`, so be warned that you might need to enter your password here or there.


# Source and inspiration

* install system/scripts and README [cowboy/dotfiles](https://github.com/cowboy/dotfiles)
* zsh split config and some config [xero/dotfiles](https://github.com/xero/dotfiles)
* zsh install and oh-my-zsh install [nicksp/dotfiles](https://github.com/nicksp/dotfiles)
* ascii art [partorjk.com](http://patorjk.com/software/taag)
* misc sources for 10+ years...

# License
The code is avaible under [MIT license](LICENSE)
