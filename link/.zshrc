#              __
#  ____  _____/ /_  __________
# /_  / / ___/ __ \/ ___/ ___/
#  / /_(__  ) / / / /  / /__
# /___/____/_/ /_/_/   \___/
#


# Path to my oh-my-zsh installation.
export ZSH=$HOME/.zsh/oh-my-zsh

# load configs
for config (~/.zsh/*.zsh) source $config
