# aliases files sumaries

# sudo
alias sudo="sudo "
alias please='sudo $(fc -ln -1)'
alias fuck='sudo $(fc -ln -1)'
alias apt="sudo apt"

# ls command
alias ls="ls -h --color=auto"	# colors and human readable
alias ll="ls -laF"		# full display
alias la="ls -A"
alias l="ls -CF"
alias countdir="ls -Al | grep -c ^d"

# cd
alias "cd.."="cd ../"
alias ..="cd ../"
alias up="cd ../"

# mv, cp, mkdir
alias rm='rm -Iv'
alias rmrf='rm -Irf'
alias mkdir='mkdir -pv'
alias cp='cp -v'

# docker
alias docker-compose='docker compose'

# git
alias g="git"
alias ga="git add"
alias gapa="git add -p"
alias gb='git branch'
alias gba='git branch -a'
alias gbl='git blame -b -w'
alias gc='git commit -v'
alias gc!='git commit -v --amend'
alias gcn!='git commit -v --no-edit --amend'
alias gco='git co'
alias gcb='git cb'
alias gd='git d'
alias gdc='git dc'
alias gst='git stat'
alias gss='git ss'
alias gl='git l'
alias gll='git ll'
alias gun='git undo'

# vim
alias v="vim"
alias iv="vim"
alias ci="vim"
alias cim="vim"
alias emacs="vim"

# python
alias py="python"
alias -s py="python"
alias py3="python3"
alias py2="python2"

# network
alias ifconfig="ip a"
alias ipconfig="ip a"
alias ipa="ip a && echo 🍺"
alias iproute="ip route && echo 💨"
alias iprout="ip route && echo 💨"
alias prout="ip route && echo 💨"

# Music
alias nvlc="nvlc --browse-dir ~/Music/"
# radio station
# alias fip="nvlc http://direct.fipradio.fr/live/fip-hifi.aac"
alias fip="mpc clear && mpc add http://direct.fipradio.fr/live/fip-hifi.aac && mpc play"
alias fip-rock="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio1.mp3 && mpc play"
alias fip-jazz="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio2.mp3 && mpc play"
alias fip-groove="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio3.mp3 && mpc play"
alias fip-world="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio4.mp3 && mpc play"
alias fip-new="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio5.mp3 && mpc play"
alias fip-reggae="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio6.mp3 && mpc play"
alias fip-electro="mpc clear && mpc add http://direct.fipradio.fr/live/fip-webradio8.mp3 && mpc play"
alias france-inter="mpc clear && mpc add http://direct.franceinter.fr/live/franceinter-midfi.mp3 && mpc play"
alias mutine="mpc clear && mpc add https://radios.infini.fr:8443/mutine && mpc play"
alias nova="mpc clear && mpc add https://radionova.ice.infomaniak.ch/radionova-256.aac && mpc play"
alias nova-classic="mpc clear && mpc add https://nova-vnt.ice.infomaniak.ch/nova-vnt-128 && mpc play"
alias nova-dance="mpc clear && mpc add https://nova-dance.ice.infomaniak.ch/nova-dance-128 && mpc play"
alias nova-nuit="mpc clear && mpc add https://nova-ln.ice.infomaniak.ch/nova-ln-128 && mpc play"
alias pbbradio="mpc clear && mpc add https://pbbradio.com:8443/pbb128 && mpc play"
alias radio-bonheur="mpc clear && mpc add https://radiobonheur.ice.infomaniak.ch/radiobonheur-128-1.mp3 && mpc play"
alias radio-u="mpc clear && mpc add http://icecast.infini.fr:8000/radiou.mp3 && mpc play"

# Misc
alias cls='clear'
alias psql='psql -U rpos'
#alias lol='base64 </dev/urandom | lolcat'
alias lol='base64 <(hexdump -c /dev/urandom | while true; do head -10|cat; sleep 0.1; done) | lolcat -a -d 1'
alias psaux="ps aux"
if [[ -f /usr/bin/htop ]]; then
	alias top="htop"
fi
alias serve="http-server"

# Sound control
alias sound-mute='pactl set-sink-mute 0 toggle'
alias sound-down='pactl set-sink-volume 0 -5% #increase sound volume'
alias sound-up='pactl set-sink-volume 0 +5% #increase sound volume'

# Functions

disks () {
	echo -e "\n\e[1m\e[38;5;220m╓───── m o u n t . p o i n t s"
	echo -e "\e[1m\e[38;5;220m╙────────────────────────────────────── ─ ─ "
	echo -en "\e[0m\e[38;5;120m"
	lsblk -a | grep -v snap
  echo -e "\n\e[1m\e[38;5;220m╓───── i n o d e s . u s a g e"
	echo -e "\e[1m\e[38;5;220m╙────────────────────────────────────── ─ ─ "
	echo -en "\e[0m\e[38;5;120m"
	df -ih | grep -v snap
	echo -e "\n\e[1m\e[38;5;220m╓───── d i s k . u s a g e"
	echo -e "\e[1m\e[38;5;220m╙────────────────────────────────────── ─ ─ "
	echo -en "\e[0m\e[38;5;120m"
	df -h | grep -v snap
}

hy () {
	history | grep -vE -e '[0-9]{1,4}  hy |history' | grep -iE -e $1 | more
}

# magic function to activate or create py3 virtualenv
activate () {
	if [[ -d ./.venv/ ]]; then
		${SPEC_PYTHON:-python3} -m venv .venv --upgrade
		source ./.venv/bin/activate
	else
		${SPEC_PYTHON:-python3} -m venv .venv --without-pip
		curl https://bootstrap.pypa.io/get-pip.py | ./.venv/bin/python
		source ./.venv/bin/activate
	fi
}
