# if in a tty console, use a patched font
case $(tty) in	/dev/tty[0-6]*)
	setfont $HOME/.local/share/fonts/ter-powerline-v14n.psf.gz
	;;
esac
