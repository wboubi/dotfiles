# a shortcut to autolock a TTY session after 2 minutes
case $(tty) in /dev/tty[0-9]*)
	export TMOUT=120
	function TRAPALRM() { vlock }
esac
