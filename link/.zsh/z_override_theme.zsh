# file to overrides function from the zsh theme

# Context: user@hostname (who am I and where am I)
prompt_context() {
  if [[ -n "$SSH_CLIENT" ]]; then
    prompt_segment cyan white "%{$fg_bold[black]%(!.%{%F{black}%}.)%}$USER@%m%{$fg_no_bold[black]%}"
  else
    prompt_segment black default "%(!.%{%F{yellow}%}.)$USER"
  fi
}


# Virtualenv: current working virtualenv
prompt_virtualenv() {
  local virtualenv_path="$VIRTUAL_ENV"
  if [[ -n $virtualenv_path && -n $VIRTUAL_ENV_DISABLE_PROMPT ]]; then
    if [[ $(basename $virtualenv_path) = '.venv' ]]; then
      prompt_segment white black "(`echo $virtualenv_path | rev | cut -d'/' -f2 | rev`)"
    else
      prompt_segment white black "(`basename $virtualenv_path`)"
    fi
  fi
}



function virtualenv_prompt_info() {
	  [[ -n ${VIRTUAL_ENV} ]] || return
	    echo "${ZSH_THEME_VIRTUALENV_PREFIX:=[}${VIRTUAL_ENV:t}${ZSH_THEME_VIRTUALENV_SUFFIX:=]}"
}

# disables prompt mangling in virtual_env/bin/activate
 export VIRTUAL_ENV_DISABLE_PROMPT=1
