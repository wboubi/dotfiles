# This is my .vimrc

All of the plugins are installed and updated via [vim-plug](http://github.com/junegunn/vim-plug)!

Have fun and fork at will! :)

# Dependances
```
curl
build-essential
python3-dev
cmake
```

# Quick start

1. Clone repo.
2. Install dependance.
3. Launch vim and wait to install.
4. If you don't understand some option, just `:help <option-name>`, Vim's documentation is wonderful!

# Fix YCM

If YCM (YouCompleteMe) is broken after a upgrade of python3 you can fix it with the vim function *ReBuildYCM* 
just execute `:call ReBuildYCM`

**Enjoy!** :)
