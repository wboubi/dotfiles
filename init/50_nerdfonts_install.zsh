# Install Nerdfont patched fonts


# Manual install because auto nerd-font repo si to big
# Set source and target directories

if [[ `uname` == 'Darwin' ]]; then
  # MacOS
  font_dir="$HOME/Library/Fonts"
else
  # Linux
  font_dir="$HOME/.local/share/fonts"
  mkdir -p $font_dir
fi

(
cd $font_dir
curl -sfLo "Fira Code Regular Nerd Font.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/FiraCode/Regular/FiraCodeNerdFont-Regular.ttf
curl -sfLo "DejaVu Sans Mono Nerd Font Complete Mono.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/DejaVuSansMono/Regular/DejaVuSansMNerdFontMono-Regular.ttf
curl -sfLo "Iosevka Light Nerd Font Complete Mono.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/Iosevka/Light/IosevkaNerdFontMono-Light.ttf
curl -sfLo "Iosevka Light Nerd Font Complete.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/Iosevka/Light/IosevkaNerdFont-Light.ttf
curl -sfLo "Iosevka Term Light Nerd Font Complete Mono.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/IosevkaTerm/Light/IosevkaTermNerdFontMono-Light.ttf
curl -sfLo "Iosevka Term Light Nerd Font Complete.ttf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/8423b722416756fb3c842ee7239c4737c5ff3bb1/patched-fonts/IosevkaTerm/Light/IosevkaTermNerdFont-Light.ttf
)


# Reset font cache on Linux
if command -v fc-cache @>/dev/null ; then
  fc-cache -f $font_dir
fi
e_success "All Nerdfonts installed to $font_dir"
