# Install rust with rustup
if command -v rustup @>/dev/null ; then
  rustup update
else
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
fi
