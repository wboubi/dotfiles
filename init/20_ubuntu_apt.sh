# Ubuntu-only stuff. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

# Update APT.
e_header "Updating APT"
sudo apt-get -qq update
sudo apt-get -qq dist-upgrade

# Install APT packages.
packages=(
  build-essential
  cmake
  curl
  figlet
  fonts-firacode
  git-core
  htop
  libssl-dev
  lolcat
  nodejs
  npm
  python-dev
  python-virtualenv
  python3-dev
  python3-venv
  toilet
  vlock
  zsh
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))

if (( ${#packages[@]} > 0 )); then
  e_header "Installing APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi
