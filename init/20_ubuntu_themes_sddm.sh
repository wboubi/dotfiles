# Ubuntu-only theme. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

if [[ ! -e /usr/bin/sddm ]]; then
  e_arrow "No sddm detected, skipping."
  return 1
fi

# Update APT.
sudo apt-get -qq update

# Install APT packages.
packages=(
  qml-module-qtquick-layouts
  qml-module-qtgraphicaleffects
  qml-module-qtquick-controls2
  libqt5svg5
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))

if (( ${#packages[@]} > 0 )); then
  e_header "Installing APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi

(
cd $DOTFILES/vendor/sddm-sugar-candy
sudo install -Dm644 "Main.qml" "/usr/share/sddm/themes/sugar-candy/Main.qml"
sudo install -Dm644 "theme.conf" "/usr/share/sddm/themes/sugar-candy/theme.conf"
sudo install -Dm644 "metadata.desktop" "/usr/share/sddm/themes/sugar-candy/metadata.desktop"
sudo find ./Assets -type f -exec install -Dm644 {} "/usr/share/sddm/themes/sugar-candy/{}" \;
sudo find ./Backgrounds -type f -exec install -Dm644 {} "/usr/share/sddm/themes/sugar-candy/{}" \;
sudo find ./Components -type f -exec install -Dm644 {} "/usr/share/sddm/themes/sugar-candy/{}" \;
)

if [[ -e /usr/share/sddm/themes/sugar-candy/theme.conf.user ]]; then
  e_arrow "sugar-candy theme.conf.user exist. skipping"
else
  (
  cd $DOTFILES/config/sugar-candy
  sudo install -Dm644 "theme.conf.user" "/usr/share/sddm/themes/sugar-candy/"
  sudo install -Dm644 "Mountain_custom.jpg" "/usr/share/sddm/themes/sugar-candy/Backgrounds/"
  )
fi

sudo update-alternatives --install /usr/share/sddm/themes/ubuntu-theme sddm-ubuntu-theme /usr/share/sddm/themes/sugar-candy 90
