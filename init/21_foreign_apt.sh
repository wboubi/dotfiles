# Ubuntu-only theme. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

e_header "Installing foreign public key"

curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo gpg --dearmour -o /usr/share/keyrings/google-keyring.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /usr/share/keyrings/docker-keyring.gpg
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmour -o /usr/share/keyrings/microsoft-keyring.gpg
curl -fsSL https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-keyring.gpg

e_header "Installing repository:"
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/google-keyring.gpg] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-keyring.gpg] http://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-keyring.gpg] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/oracle-virtualbox.list

packages=(
  google-chrome-stable
  docker-ce
  docker-ce-cli
  containerd.io
  docker-buildx-plugin
  docker-compose-plugin
  code
  virtualbox-7.0
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))
sudo apt-get -qq update

if (( ${#packages[@]} > 0 )); then
  e_header "Installing PPA-APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi
