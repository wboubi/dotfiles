# Ubuntu-only theme. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

#
ppas=(
  "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  "deb http://repository.spotify.com stable non-free"
  "deb [arch=amd64] http://packages.microsoft.com/repos/vscode stable main"
)
e_header "Installing foreign public key"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0DF731E45CE24F27EEEB1450EFDC8610341D9410
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -

e_header "Installing PPA repository: ${ppas[*]}"
for ppa in "${ppas[@]}"; do
  sudo add-apt-repository -y -u "$ppa"
done

packages=(
  google-chrome-stable
  docker-ce
  spotify-client
  code
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))

if (( ${#packages[@]} > 0 )); then
  e_header "Installing PPA-APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi
