# Install Oh My Zsh if it isn't already present
export ZSH=$HOME/.zsh/oh-my-zsh

if [[ ! -d $ZSH ]]; then
  e_header "Installing Oh-my-zsh"
  git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git $ZSH || {
    e_error "Git clone of oh-my-zsh repo failed."
    return 1
  }
else
  e_arrow "Already installed. Skipping."
fi

# Set the default shell to zsh if it isn't currently set to zsh
if [[ ! $(echo $SHELL) == $(which zsh) ]]; then
  e_header "Setup default shell to zsh"
  ME=`whoami`
   sudo chsh -s $(which zsh) $ME
fi

