# Ubuntu-only theme. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

#
ppas=(
#  ppa:noobslab/themes
  ppa:numix/ppa
)

e_header "Installing PPA repository: ${ppas[*]}"
for ppa in "${ppas[@]}"; do
  sudo add-apt-repository -y -u "$ppa"
done

packages=(
#  arc-flatabulous-theme
  numix-icon-theme-square
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))

if (( ${#packages[@]} > 0 )); then
  e_header "Installing PPA-APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi
