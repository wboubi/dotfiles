# Install Powerline patched fonts
powerline_fonts_dir=$HOME/.zsh/fonts

if [[ ! -d $powerline_fonts_dir ]]; then
  e_header " Installing Powerline patched fonts"
  git clone https://github.com/powerline/fonts.git $powerline_fonts_dir || {
    e_error "Git clone of powerline/fonts repo failed."
    return 1
  }
fi

# Manual install because auto don't install psf fonts
# Set source and target directories

if [[ `uname` == 'Darwin' ]]; then
  # MacOS
  font_dir="$HOME/Library/Fonts"
else
  # Linux
  font_dir="$HOME/.local/share/fonts"
  mkdir -p $font_dir
fi

find $powerline_fonts_dir \( -name '*.[o,t]tf' -or -name '*.p[c,s]f.gz' \) -type f -print0 | xargs -0 -I % cp "%" "$font_dir/"

# Reset font cache on Linux
if command -v fc-cache @>/dev/null ; then
  fc-cache -f $font_dir
fi
e_success "All Powerline fonts installed to $font_dir"
