# Ubuntu-only stuff. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

# Update APT.
sudo apt-get -qq update

# Install APT packages.
packages=(
  i3
  xautolock
)

packages=($(setdiff "${packages[*]}" "$(dpkg --get-selections | grep -v deinstall | awk '{print $1}')"))

if (( ${#packages[@]} > 0 )); then
  e_header "Installing APT packages: ${packages[*]}"
  for package in "${packages[@]}"; do
    sudo apt-get -qq install "$package"
  done
fi
