# Ubuntu-only theme. Abort if not Ubuntu.
is_ubuntu || return 1

if [[ ! check_sudo ]]; then
  e_error "No super power avalaible."
  return 1
fi

e_header "Installing postgres public key"
curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/postgresql-keyring.gpg

e_header "Installing repository:"
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/postgresql-keyring.gpg] https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
sudo apt-get -qq update

e_success "You can install the desired postgres now"
